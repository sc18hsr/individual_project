#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import rospy
import rospkg
import cv2
import os
from std_msgs.msg import String
import sys
from pathlib import Path
from clothing_colour import *

PATH = str(Path(os.path.dirname(os.path.abspath(__file__))).parents[0])

#give info on user <name>

def new_user(name, image, time):
    #add to attendence list
    Info = open("attendence.txt", "a+")
    ID = len(open("attendence.txt").readlines(  )) + 1
    colour = get_features(image)
    Info.write(str(ID) + "," + name + "," + colour + "," + str(time) + "\n")
    print ("\n[INFO] Person attendence ID = " + str(ID))
    Info.close()
    save_new_user(ID, name, image)


def save_new_user(id, name, image):
    #save user's image
    print("saving user image")
    cv2.imwrite(PATH + "/src/data/people/Person." + str(id) + '.' + name + ".jpg " , image)

def get_features(image):
    print("getting hair colour and clothing colour")
    img, colour = detect_clothing(image)
    return colour

def give_info_to_user(name):
    INFO = []
    with open(PATH + "/src/information.txt", "r") as f :
        for line in f:
            INFO.append (line.split(",")[1].rstrip())
    print(INFO)
