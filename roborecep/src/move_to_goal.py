#!/usr/bin/python

#include libraries and import messages
import re
import rospy
import actionlib
from std_msgs.msg import Header, Bool, String
from geometry_msgs.msg import Point, Pose
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal


class ToGoal:
    def __init__(self):
        self.xp = 0
        self.yp = 0
        self.move_flag = False
        self.save_costmaps_state = None

        #create a simple action client
        self.move_base_client = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        self.move_base_client.wait_for_server()

    #moves the robot to point (x,y)
    def move_to_point(self, x, y):

        goal = MoveBaseGoal()
        #obtain map
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()

        #create a new goal pose
        goal_pose = Pose()
        goal_pose.position.x = x
        goal_pose.position.y = y
        goal_pose.orientation.x = 0
        goal_pose.orientation.y = 0
        goal_pose.orientation.z = 0
        goal_pose.orientation.w = 1

        goal.target_pose.pose = goal_pose

        # send the goal
        self.move_base_client.send_goal(goal)
        #wait for gaol
        success = self.move_base_client.wait_for_result()
        #get the state of the goal
        state = self.move_base_client.get_state()
        result = False

        # if goal is reaches
        if success == actionlib.GoalStatus.SUCCEEDED:
            rospy.loginfo('GOAL REACHED! (:')
            result = True

        return result

    # escort user to given point on map
    def move(self, x, y):
        header = Header(frame_id = 'map', stamp = rospy.Time.now())

        # print on goal sent
        rospy.loginfo('GOAL SENT! o:')
        success = self.move_to_point(x,y)

        #print on finish
        rospy.loginfo('GOAL REACHED! :)')



if __name__ == '__main__':
    rospy.init_node('move_to_goal')
    try:
        TG = ToGoal()
        #TESTING
        TG.move(-7.33, 2.45)

    except rospy.ROSInterruptException:
        pass
