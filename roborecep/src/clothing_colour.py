#!/usr/bin/python
#include files
from __future__ import division
from __future__ import print_function
import cv2
import sys
import numpy as np
import matplotlib
import math
matplotlib.use("agg")
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from collections import Counter
import webcolors

# Load the cascade
face_cascade = cv2.CascadeClassifier('./Haar_Cascades/haarcascade_frontalface_default.xml')

# compress the imagee
def compress(frame):
	Z = frame.reshape((-1, 3))
	Z = np.float32(Z)
	# define criteria, number of clusters(K) and apply kmeans()
	criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
	K = 5

	ret, label, center = cv2.kmeans(Z, K, None, criteria, 10,
									cv2.KMEANS_RANDOM_CENTERS)

	# Now convert back into uint8, and make original image
	center = np.uint8(center)
	res = center[label.flatten()]
	res2 = res.reshape((frame.shape))
	return res2

# segment clothing region
def cloth_region(blur, shirt_x, shirt_y, shirt_h, shirt_w, name='hari'):
	crop_frame = blur[shirt_y:shirt_y+shirt_h, shirt_x:shirt_x+shirt_w]
	crop_frame = compress(crop_frame)

	avg_color_per_row = np.average(crop_frame, axis=0)
	avg_color = np.average(avg_color_per_row, axis=0)

	r, g, b = map(lambda x: int(x) if not math.isnan(x) else x,
				  [avg_color[2], avg_color[1], avg_color[0]])

	cv2.imwrite('cropped.jpg', crop_frame)
	colour = d_detect(crop_frame)
	#print (colour)
	diff = abs(g-b)
	return True if diff <= 20 and max(g, b) - 20 >= r else False, colour

#find the closent colour wthh a name
def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.CSS3_HEX_TO_NAMES.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]


def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name

#using k-means clustering to find the dominant colour,
#returns the name of the colour
def d_detect(result):
	image = result.reshape((result.shape[0] * result.shape[1], 3))
	k=4
	image_processing_size = None
	#cluster and assign labels to the pixels
	clt = KMeans(n_clusters = k)
	labels = clt.fit_predict(image)
    #count labels to find most popular
	label_counts = Counter(labels)
   	#subset out most popular centroid
	dominant_color = clt.cluster_centers_[label_counts.most_common(1)[0][0]]

	colour_name, closest_name = get_colour_name(dominant_color)
	#print(closest_name)
	return closest_name

# detect the face region annd regions for cloth segmentation
def detect_clothing(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(frame, (7, 7), 0)

    detected = False
    sensitivity = False
    colour = None

    # Detect faces
    faces = face_cascade.detectMultiScale(gray, 1.2, 5)
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)
        detected = True
        #print(x, y, w, h)
        # Shirt coordinates
        offset_y_bottom = 70
        offset_x_left = 30
        offset_x_right = 30
        shirt_y = y + h + offset_y_bottom
        shirt_w = w // 3
        shirt_h = w // 3

        # shirt region 1 (left)
        shirt_x = x + (w // 3) - offset_x_left
        sensitivity_1,colour = cloth_region(blur, shirt_x, shirt_y, shirt_h, shirt_w)
        cv2.rectangle(frame, (shirt_x, shirt_y),
                      (shirt_x+shirt_w, shirt_y+shirt_h), (255, 0, 0), 2)
        #print(colour)

        # shirt region 2 (right)
        shirt_x = x + (w // 3) + offset_x_right
        sensitivity_2 = cloth_region(blur, shirt_x, shirt_y, shirt_h, shirt_w)
        cv2.rectangle(frame, (shirt_x, shirt_y),
                      (shirt_x+shirt_w, shirt_y+shirt_h), (255, 0, 0), 2)

        # shirt region 3 (bottom)
        shirt_y = y + h + 130
        shirt_x = x + (w // 3)
        sensitivity_3 = cloth_region(blur, shirt_x, shirt_y, shirt_h, shirt_w)
        cv2.rectangle(frame, (shirt_x, shirt_y),
                      (shirt_x+shirt_w, shirt_y+shirt_h), (255, 0, 0), 2)

        sensitivity = sensitivity_1 or sensitivity_2 or sensitivity_3
        break

    return frame, colour
