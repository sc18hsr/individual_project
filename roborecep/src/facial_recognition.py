#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import cv2
import os
import sys
import argparse as arg
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
import face_recognition
from pathlib import Path

PATH = str(Path(os.path.dirname(os.path.abspath(__file__))).parents[0])

#open an d read from file
def FileRead():
    NAME = []
    with open("users_name.txt", "r") as f :
        for line in f:
            NAME.append (line.split(",")[1].rstrip())
    return NAME

#get username from username
def Get_UserName(ID, conf):
    if not ID > 0:
        return "unknown"
    return FileRead()[ID -1]

# detect faces and return the detcted user's name and image
def recognise_face(im, close_count = 0, open_count = 0, detectblink = False):

    #create LBPH recogniser 
	recognizer = cv2.face.LBPHFaceRecognizer_create()
	recognizer.read(PATH + '/src/newtrain.yaml')

    # import cascade classifiers
	faceCascade = cv2.CascadeClassifier(PATH + '/src/Haar_Cascades/haarcascade_frontalface_default.xml')
	open_eyes_detector = cv2.CascadeClassifier(PATH + '/src/Haar_Cascades/haarcascade_eye_tree_eyeglasses.xml')
	left_eye_detector = cv2.CascadeClassifier(PATH + '/src/Haar_Cascades/haarcascade_lefteye_2splits.xml')
	right_eye_detector = cv2.CascadeClassifier(PATH +'/src/Haar_Cascades/haarcascade_righteye_2splits.xml')

    #convert imiage to grey
	gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
	rgb = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
	name = 'not found'
    #detect faces
	faces = faceCascade.detectMultiScale(gray, 1.2, 5)

	for (x,y,w,h) in faces:
		#cv2.rectangle(im,(x-50,y-50),(x+w+50,y+h+50),(255,0,0),2)
		cv2.rectangle(im, (x, y), (x+w, y+h), (0, 255, 0), 2)
		# Compare the histogram of current image with all known faces
		idx, confidence = recognizer.predict(gray[y:y+h,x:x+w])
		name = 'unknown'
		if(confidence < 50):
			name = Get_UserName(idx, confidence)

		#print("Found: ", idx, "name:", name)
		#print("Confidence: ", confidence)
		cv2.putText(im, name, (x, y), cv2.FONT_HERSHEY_SIMPLEX,0.75, (0, 255, 0), 2)

		if detectblink == True:

					# eye detection
					eyes = []
					open_eyes_glasses = open_eyes_detector.detectMultiScale(
			            gray,
			            scaleFactor=1.1,
			            minNeighbors=5,
			            minSize=(30, 30),
			            flags = cv2.CASCADE_SCALE_IMAGE
			        )

					# if open_eyes_glasses detect eyes then they are open
					if len(open_eyes_glasses) == 2:
						open_count +=1
						for (ex,ey,ew,eh) in open_eyes_glasses:
							cv2.rectangle(im,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

					#if both eyes are not open
					else:
						close_count+=1
						left_face = im[y:y+h, x+int(w/2):x+w]
						left_face_gray = gray[y:y+h, x+int(w/2):x+w]
						right_face = im[y:y+h, x:x+int(w/2)]
						right_face_gray = gray[y:y+h, x:x+int(w/2)]

						left_eye = left_eye_detector.detectMultiScale(
							left_face_gray,
							scaleFactor=1.1,
							minNeighbors=5,
							minSize=(30, 30),
							flags = cv2.CASCADE_SCALE_IMAGE
			            )
						right_eye = right_eye_detector.detectMultiScale(
							right_face_gray,
							scaleFactor=1.1,
							minNeighbors=5,
							minSize=(30, 30),
							flags = cv2.CASCADE_SCALE_IMAGE
							)

                        #draw rectangles on the eyes
						for (ex,ey,ew,eh) in right_eye:
							color = (0,0,255)
							cv2.rectangle(right_face,(ex,ey),(ex+ew,ey+eh),color,2)
						for (ex,ey,ew,eh) in left_eye:
							color = (0,0,255)
							cv2.rectangle(left_face,(ex,ey),(ex+ew,ey+eh),color,2)

	return im, name, close_count, open_count


if __name__ == "__main__":
    print ("main!")
