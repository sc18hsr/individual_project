#!/usr/bin/python
from __future__ import division
from __future__ import print_function


#import required packages
import rospy
import rospkg
import cv2
import os
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import sys
import numpy as np
import face_recognition
from pathlib import Path

#import functions
from facial_recognition import *
from move_to_goal import *
from give_info import *


#class receptionist
#invokes functions from other modules
class receptionist:
    def __init__(self):
        self.PATH = str(Path(os.path.dirname(os.path.abspath(__file__))).parents[0])

        #cv bridge
        self.bridge = CvBridge()

        #stores the imag of the current user
        self.current_img = None

        #number of times the user was detected
        self.count = 0

        #name of the current user
        self.current = None

        #name of the previos user
        self.prev = None

        #status:
        # FIND = LOOK FOR USERS
        # NAV = NAVIGATION
        self.STATUS = 'FIND'

        #subscriber to image topic
        self.image_sub = rospy.Subscriber("/xtion/rgb/image_raw", Image,self.callback)

        #coordinates of the rooms
        self.room1x=7.48
        self.room1y=2.59
        self.room2x=-7.33
        self.room2y=2.45
        self.room3x=2.61
        self.room3y=3.9
        self.waitingx=-4.61
        self.waitingy=-5.38

    # asks for place to go to or
    # moves to waiting room if no point is specified
    # escort new user to waiting room and known user to the place they want
    def move_to(self, topoint = "waiting_area"):
        TG = ToGoal()
	now = rospy.get_time()
        #new user
        if self.current == "unknown":
            print("Hi! I am TIAGo, may I know your name?")
            name = input('\nName: > ')
            new_user(name, self.current_img,now)
            while self.STATUS == NAV:
                ans = input('\nCan I escort you to the waiting area?:  ')
                if ans == "yes":
                    TG.move(self.waitingx, self.waitingy)
                    TG.move(0,0)
                    self.STATUS = 'FIND'
                elif ans =="no":
                    print("It was good to see you here, bye")
                    self.STATUS = 'FIND'
                else:
                    print("[INFO] Invalid Input")

        #known user
        else:
            print("Hi", self.current)
            new_user(self.current, self.current_img,now)
            while self.STATUS == NAV:
                give_info(self.current)
                loc = input('\nEnter room number (1-3): ')
                print("[INFO] escorting user to room", loc)
                if loc == 1:
                    TG.move(self.room1x,self.room1y)
                    TG.move(0,0)
                    STATUS = 'FIND'
                elif loc == 2:
                    TG.move(self.room2x, self.room2y)
                    TG.move(0,0)
                    STATUS = 'FIND'
                elif loc == 3:
                    TG.move(self.room3x, self.room3y)
                    TG.move(0,0)
                    STATUS = 'FIND'
                else:
                    print("[INFO] Invalid room number")


    # open text file and provide specific information to the user
    def give_info(self, name):
        print("[INFO FROM ADMIN]")
        give_info_to_user(name)


    # callback function for processing images
    def callback(self, data):
        if self.STATUS == 'FIND':
            print("[LOG] waiting for user, image recieved from camera")
            try:
                #convery image to cv imsge
                img = self.bridge.imgmsg_to_cv2(data, "bgr8")

                #face recognition
                img, name, x, y = recognise_face(img)

                if name != 'not found':
                    print("[INFO] person found")
                    print("[INFO] person's name is", name)

                    #check if same person is detected again
                    self.current_img = img
                    if self.prev == None:
                        self.prev = name
                        self.current = name
                    else:
                        self.prev = self.current
                        self.current = name

                    if self.prev == self.current:
                        self.count += 1

                    #display the deteection
                    cv2.namedWindow('camera_Feed')
                    cv2.imshow('camera_feed', img)
                    cv2.waitKey(3)
                    cv2.destroyAllWindows()
                    if self.count > 5:
                        self.STATUS = 'NAV'
                        self.count=0

            except CvBridgeError as e:
                print(e)
        elif self.STATUS =='NAV':
            self.move_to()
            self.STATUS = 'END'
        else:
            #display the image of the last detcted user
            cv2.namedWindow('last person')
            cv2.imshow('last_person', self.current_img)
            cv2.waitKey(3)


    # unsubribe from image topics
    def unsubscribe(self):
        self,image_sub.unregister()


#main
def main(args):
    rospy.init_node('re_server', anonymous=True)
    print("[LOG] server initialised, waiting for a command")

    REC = receptionist()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("shutting down ")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
